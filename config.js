
import Icon from './img/abcd.svg';
const meta = require('./locales/Index.json');
let infos = {'title': {}, 'subtitle': {}}
for (const lang in meta) {
	if ('title' in meta[lang]) {
		infos['title'][lang] = meta[lang]['title'];
	} else {
		infos['title'][lang] = "";
	}
	if ('subtitle' in meta[lang]) {
		infos['subtitle'][lang] = meta[lang]['subtitle'];
	} else {
		infos['subtitle'][lang] = "";
	}
}

export default {
	id: "abcd",
	meta: {
		title: infos['title'],
		text: infos['subtitle'],
		to: 	"abcd-index",
		adminto:"abcd-admin",
		role:	"abcd",
		icon: 	Icon,
		index:	true,
	},
	routes: [
		{	path: '/abcd-index', name:'abcd-index', component: () => import('./views/Index.vue') },
		{	path: '/abcd/:key', name:'abcd-login-key', component: () => import('./views/Part.vue') },
		{	path: '/abcd/', name:'abcd-login', component: () => import('./views/Part.vue') },
		{	path: '/abcd-run', name:'abcd-run', component: () => import('./views/Run.vue') },
		{	path: '/abcd-show/:ticket', name:'abcd-show', component: () => import('./views/Show.vue') },
		{	path: '/abcd-edit', name:'abcd-edit', component: () => import('./views/Edit.vue') },
		{	path: '/abcd-admin', name:'abcd-admin', component: () => import('./views/Admin.vue') },
	],
	imageMaxSize:102400
}
