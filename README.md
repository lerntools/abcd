<a href="https://translate.codeberg.org/engage/lerntools/">
<img src="https://translate.codeberg.org/widgets/lerntools/-/abcd/svg-badge.svg" alt="Übersetzungsstatus" />
</a>

# abcd

An ABCD quiz consists of a series of questions, each with 4 answers - but only one of these answers is correct. All participants answer the questions one after the other, with only a certain amount of time allowed for each question.

This repository is an optional module for the "lerntools". For installation and configuration, please see the documentation in https://codeberg.org/lerntools/base

We are planning a rework of this module atm. We will keep the current functionality and improve this module by adding support for questions on the end devices.
