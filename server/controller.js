//External libs
const { check,body,oneOf,validationResult }=require('express-validator/check');
const { sanitize }=require('express-validator/filter');
const debug=require('debug')('abcd');
const cron=require('node-cron');
const generator=require('../../../main/server/passwordHelper');
const path=require('path');

//Internal libs
const consts=require ('../../../consts.js');
const mainController=require('../../../main/server/controller');
const userHelper=require('../../../main/server/userHelper');

//Database schemas
var AbcdSet=require('./modelAbcd');
var AbcdInstance=require('./modelAbcdInstance');

const TICKET_LENGTH=10;
const KEY_LENGTH=8;
const STATES=['lobby','ask','answer','report','finish']

/********************************************************************************
 Common functions
 ********************************************************************************/

/*
 Load abcd set from id param in request
 */
exports.loadSetFromId=[
	sanitize('id').trim().escape(),
	(req, res, next)=> {
		var id=req.query.id || req.body.id || req.params.id;
		if (!id) return res.json([{msg:'not-available'}]);
		AbcdSet.findOne({_id: id}).populate('parts').exec(function(err,abcd) {
			if (err) debug(err);
			if (!abcd) return res.json([{msg:'not-available'}]);
			res.locals.abcd=abcd;
			next();
		});
	}
]

/*
 Load instance from key param in request
 */
exports.loadInstanceFromKey=[
	sanitize('key').trim().escape(),
	(req, res, next)=> {
		var key=req.query.key || req.body.key || req.params.key;
		if (!key) return res.json([{msg:'not-available'}]);
		AbcdInstance.findOne({ key: key }).exec(function (err, abcdInstance) {
			if (err) debug(err);
			if (!abcdInstance) return res.json([{msg: "not-available"}]);
			res.locals.abcdInstance=abcdInstance;
			next();
		})
	}
]

/*
 Check authorization
 */
exports.checkAuth=function(req, res, next) {
   mainController.checkAuthAndRole(req,res,next,"abcd");
}


/********************************************************************************
Teacher
********************************************************************************/

/*
Return meta information on visible sets for teacher
- Authentication via checkAuth (route)
- further authorization with db request
*/
exports.getSetsMeta=function(req, res, next) {
	AbcdSet.find({ $or:[ {owner:res.locals.user.login}, {visible:true}]},'title desc owner subject ticket visible').sort({ subject: 'asc', title: 'asc'}).exec(function(err,allAbcdSets) {
		res.json(allAbcdSets);
	});
}

/*
Get set by id
- Authentication via checkAuth (route)
- further authorization with db request
*/
exports.getSet=[
	sanitize('id').trim().escape(),
	function(req, res, next) {
		var id=req.query.id || req.body.id || req.params.id;
		AbcdSet.findOne({ _id:id }).exec(function(err,abcd) {
			if (err || !abcd) return res.json({});
			if (abcd.owner==res.locals.user.login || abcd.visible) return res.json(abcd);
			return res.json({});
		});
	}
]

/*
Start quiz
- Authentication via checkAuth (route)
- further authorization via quiz properties
*/
exports.newInstance=[
	body('title').trim().matches(consts.REGEX_TEXT_100),
	(req, res, next)=> {
		var key=generator.generate({length: KEY_LENGTH, numbers: true});
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var abcdInstance=new AbcdInstance({owner:res.locals.user.login, title: req.body.title, participants: [], key: key, currentQuestion:-1, state:'lobby'});
		abcdInstance.save(function(err) {
			if (err) {
				debug(err);
				return res.json([{msg:'database-error'}]);
			}
			return res.json(abcdInstance);
		});
	}
]

/*
 Show participant list
 - Authentication via checkAuth (route)
 */
exports.getParticipants=function(req,res,next) {
	if (res.locals.user.login!=res.locals.abcdInstance.owner) return res.json([{msg:'not-allowed'}]);
	return res.json(res.locals.abcdInstance.participants);
}

/*
 Kick participant
 - Authentication via checkAuth (route)
 - Authorization via loadInstanceFromSession (route)
 */
exports.delParticipant=[
	sanitize('partid').trim().escape(),
	(req, res, next)=> {
		if (res.locals.user.login!=res.locals.abcdInstance.owner) return res.json([{msg:'not-allowed'}]);
		var partid=req.params.partid;
		var participants=res.locals.abcdInstance.participants;
		participants.splice(participants.findIndex(function(i){ return i._id==partid; }), 1);
		res.locals.abcdInstance.save(function(err) {
			res.json({message: "participant-deleted"});
		});
	}
]

/*
 Set state of instance
 */
exports.setState=[
	sanitize('state').trim(),
	body('pos').trim().isInt({min:-1,max:1000}),
	(req, res, next)=> {
		if (res.locals.user.login!=res.locals.abcdInstance.owner) return res.json([{msg:'not-allowed'}]);
		var state=req.body.state;
		var pos=req.body.pos;
		if (STATES.indexOf(state)<0) return res.json([{msg:'validation-error'}]);
		//Update state
		if (state=='finish') {
			AbcdInstance.deleteMany({key:req.params.key}, function(err) {
				if (err) debug(err);
				return res.json({});
			})
		}
		else {
			AbcdInstance.findOneAndUpdate(
				{ key: req.params.key },
				{ $set: { state:state, currentQuestion:pos}},
				function(err) {
					if (err) debug(err);
					return res.json({});				}
			);
		}
	}
]


/*
 Return votes
 - Authentication via checkAuth (route)
 */
exports.getVotes=function(req,res,next) {
	if (res.locals.user.login!=res.locals.abcdInstance.owner) return res.json([{msg:'not-allowed'}]);
	var abcdInstance=res.locals.abcdInstance;
	res.json(abcdInstance.votes);
}


/********************************************************************************
Participants functions
********************************************************************************/


/*
 load set from ticket in request
 */
exports.getSetByTicket=[
	sanitize('ticket').trim().escape(),
	(req, res, next)=> {
		var ticket=req.params.ticket;
		if (!ticket || ticket=="") return res.json([{msg:'not-available'}]);
		AbcdSet.findOne({ticket:ticket}, function(err,abcd) {
			if (err) debug(err);
			if (!abcd) return res.json([{msg:'not-available'}]);
			res.locals.abcd=abcd;
			res.json(abcd);
		})
	}
]

/*
 Add participant
 - Authentification and authorization with key
 */
exports.login=[
	body('nickname').matches(consts.REGEX_NICKNAME),
	sanitize('key').trim().escape(),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		res.locals.page=req.body;
		var key=req.body.key;
		var nickname=req.body.nickname;
		AbcdInstance.findOne({ key: key }).exec(function (err, abcdInstance) {
			//Cancel if quiz is not available
			if (err || abcdInstance==null) return res.json([{msg:"not-available"}]);
			//Cancel it game is already running
			if (abcdInstance.state!='lobby') return res.json([{msg: "already-running"}]);
			//Cancel if name already in use
			for (var i=0; i<abcdInstance.participants.length; i++) {
				if (abcdInstance.participants[i].name==nickname) {
					return res.json([{msg:'nickname-taken'}]);
				}
			}
			//Add participant to db
			var token=generator.generate({length: 32, numbers: true});
			var newParticipant={name:nickname,points:0,token:token};
			AbcdInstance.findOneAndUpdate({ _id: abcdInstance._id }, { $push: { participants: newParticipant }}, function(err) {
				if (err) return res.json([{msg: "not-available"}]);
				return res.json({token:token});
			})
		})
	}
]

/*
 Vote
 - Authorization via loadInstanceFromSession (route)
 */
exports.vote=[
	body('option').matches(/^0|1|2|3$/),
	body('nickname').matches(consts.REGEX_NICKNAME),
	body('token').matches(consts.REGEX_TEXT_50),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json({message: "invalid-option"});
		var option=req.body.option;
		var nickname=req.body.nickname;
		var token=req.body.token;
		var abcdInstance=res.locals.abcdInstance;
		//search Participants
		var index=abcdInstance.participants.findIndex(function(i){ return i.name==nickname; });
		if (index<0) return res.json({message: "you-are-banned"});
		if (abcdInstance.participants[index].token!=token) res.json({message: "you-are-banned"});
		//Cancel if not question active
		if (abcdInstance.state!="ask") return res.json({message: "wait-for-question"});
		//Cancel if question already answered
		for (var i=0; i<abcdInstance.votes.length; i++) {
			var vote=abcdInstance.votes[i];
			if (vote.participant==nickname && vote.question==abcdInstance.currentQuestion)
				return res.json({message: "already-answered"});
		}
		//Save answer
		AbcdInstance.findOneAndUpdate(
			{ _id: abcdInstance._id },
			{ $push: { votes: {participant:nickname, question: abcdInstance.currentQuestion, option: option}}},
			function(err) {
				if (err) return res.json({message: "save-error"});
				return res.json({message: "confirm-vote"});
			}
		)
	}
]



 /********************************************************************************
 Author functions
 ********************************************************************************/

 /*
  Delete set
  - Authentication via checkAuth (route)
  - Authorization via checkAuth, loadSetFromId and ensureUserIsOwner (route)

  */
exports.deleteSet=function(req,res,next) {
	var abcd=res.locals.abcd;
	if (res.locals.user.login!=abcd.owner && !userHelper.checkUserRole(res,'admin')) return res.json([{msg:'not-allowed'}]);
 	AbcdSet.deleteOne({_id:abcd._id}, function(err) {
		if (err) return res.json([{msg:'database-error'}]);
		return res.json({});
 	})
 }

 /*
  Create new ABCD Set
  - Authentication via checkAuth (route)
  - no further authorization
  */
exports.addNewSet=function(req,res,next) {
	var data=req.body.abcd;
 	//Cancel if file is missing
 	if (!data || !data.title) return res.json([{msg:'no-file-selected'}]);
 	//Validate meta data
 	var abcd={};
 	abcd.owner=res.locals.user.login;
	parseJson(data,abcd);
 	var newAbcd=AbcdSet(abcd);
 	var errors=[];
 	//Avoid duplicate tickets
 	AbcdSet.findOne({ticket:newAbcd.ticket}, function(err,abcdWithSameTicket) {
 		if (err) debug(err);
 		if (abcdWithSameTicket && newAbcd.ticket) newAbcd.ticket=generator.generate({length: TICKET_LENGTH, numbers: true});
 		//Save
 		newAbcd.save(
 			function(err) {
 				if (err) {
 					debug(err);
 					errors.push({msg:'quiz-not-imported'});
 				}
 				return res.json(errors);
 			}
 		)
 	})
 }

 /*
  Update existing ABCD Set
  - Authentication via checkAuth (route)
  - no further authorization
  */
exports.updateSet=function(req,res,next) {
	var data=req.body.abcd;
	//Cancel if file is missing
	if (!data || !data._id) return res.json([{msg:'validation-error'}]);
	AbcdSet.findOne({ $and: [ {_id:data._id}, {owner:res.locals.user.login}]}).exec(function (err, abcd) {
		if (err || !abcd) return res.json([{msg:'database-error'}]);
		parseJson(data,abcd);
		//Avoid duplicate tickets
		AbcdSet.findOne({ticket:abcd.ticket}, function(err,abcdWithSameTicket) {
			if (abcdWithSameTicket && abcd.ticket && abcdWithSameTicket._id.toString()!=abcd._id.toString()) {
		 		return res.json([{msg:'ticket-not-imported'}]);
			}
			//Finally save
			abcd.save(function(err) {
	 			if (err) {
					debug(err);
					return res.json([{msg:'database-error'}]);
				}
				return res.json({});
	 		});
		})
	 })
 }

/*
 Parse Json-Input (data) and copy contents to abcd-object
 */
function parseJson(data,abcd) {
	if (data.title && data.title.match(consts.REGEX_TEXT_100)) 		abcd.title=data.title;
 	else { abcd.title=''; }
 	if (data.subject && data.subject.match(consts.REGEX_TEXT_100))		abcd.subject=data.subject;
 	else {  abcd.subject=''; }
 	if (data.desc && data.desc.match(consts.REGEX_TEXT_300))		abcd.desc=data.desc;
 	else {  abcd.desc=''; }
 	if (data.ticket && data.ticket.match(consts.REGEX_TICKET))			abcd.ticket=data.ticket;
 	else { abcd.ticket='' }
 	abcd.visible=(data.visible=='true' ? true : false);
 	//Validierung der Fragen
 	abcd.questions=[];
 	if (data.questions) {
 		for (dq of data.questions) {
 			var question={};
 			if (dq.text && dq.text.match(consts.REGEX_TEXT_300))	question.text=dq.text;
 			else {  question.text=''; }
 			if (dq.info && dq.info.match(consts.REGEX_TEXT_AREA))	question.info=dq.info;
 			else {  question.info=''; }
 			var options=[];
 			for (var i=0; i<4; i++) {
 				if (dq.options && dq.options[i] && dq.options[i].match(consts.REGEX_TEXT_300)) options.push(dq.options[i]);
 				else { options.push(''); }
 			}
 			question.options=options;
 			if (dq.answer>=0 && dq.answer<=4) question.answer=dq.answer;
 			else { question.answer=-1; }
 			if (dq.time && dq.time>5 && dq.time<=300) question.time=dq.time;
 			else { question.time=300; }
 			if (dq.image && dq.image.match(consts.REGEX_DATA_URI) && dq.image.length<consts.MAX_INLINE_IMAGE_SIZE*1.4) question.image=dq.image;
 			else dq.image="";
 			abcd.questions.push(question)
 		}
 	}
}


 /********************************************************************************
  Admin
  ********************************************************************************/

  /*
   Index page of admin
   - Authorization via route
   */
exports.getSetsMetaAdmin=function(req, res, next) {
 	AbcdSet.find({},'title desc owner subject ticket visible').sort({ owner: 'asc', title: 'asc'}).exec(function(err,allAbcdSets) {
 		if (err) debug(err);
		res.json(allAbcdSets);
 	});
}


/********************************************************************************
 Utils
 ********************************************************************************/

/*
Delete all user data - only used from user controller
*/
exports.deleteUserData=function(login) {
	return new Promise(resolve=> {
		AbcdSet.deleteMany({owner:login}, function(err) {
	 		if (err) debug(err);
	 		resolve();
	 	})
	})
}

/*
 Clean up database on a regular base
 */
cron.schedule("03 03 * * *", deleteAbcdInstances);
function deleteAbcdInstances() {
	AbcdInstance.deleteMany({}, function(err) {
    if (err) debug(err);
    debug("Deleting old abcd games");
	})
}
