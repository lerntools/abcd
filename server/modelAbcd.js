var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var AbcdQuestionSchema=new Schema({
	text: {type: String, required: true, max: 300},
	options: [
		{type: String, required: true, max: 300},
		{type: String, required: true, max: 300},
		{type: String, required: true, max: 300},
		{type: String, required: true, max: 300}
	],
	answer: {type: Number, required: true},
	info: {type: String, required:false, max: 300, default: ""},
	image: {type: String, required:false},
	time: {type: Number, required: true},
})

var AbcdSetSchema=new Schema({
	title :		{type: String, required: true, max: 300},
	desc :		{type: String, max: 300},
	owner:		{type: String, max: 300},
	subject:	{type: String, max: 300, default: ""},
	ticket:		{type: String, max: 300, default: ""},
	visible:	{type: Boolean, default: false},
	questions:	[AbcdQuestionSchema]
});

module.exports=mongoose.model('AbcdSet', AbcdSetSchema );
