Hvis du ikke ønsker, at ABCD-quizzen skal deles via et link, skal du slette adgangsbilletten.

Hvis quizzen kun skal være tilgængelig for dig, skal du markere den som "privat".

Spørgsmålene kan flyttes ved hjælp af de lodrette pile.