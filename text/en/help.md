# ABCD-Quiz

## Instructions for authors


The "ABCD Quiz" is a mixture of the audience question on "Who Wants to be a Millionaire?" and the "Traffic Light Quiz". A game consists of several questions, each of which has four given answers (A, B, C and D), of which only one is correct. The question is presented to all participants at the same time (on the projector), and they choose the answer in a "secret ballot" on their smartphones. The time allowed for answering a question is limited. The moderator and participants do not need any special software, an up-to-date web browser is sufficient.



## Start a game

Only a person registered on the website with the appropriate authorization, can start as a moderator:in a new game or even create new questions:

1. log in to the website (important, otherwise the ABCD quiz will appear in the participants' view) 
2. navigate to the item ABCD-Quiz

![](text/en/abcd/example1.png){.center}

3. click on the symbol to start (triangle)

![](text/en/abcd/example2.png){.center}

All self-created question collections are available, as well as those that have been marked as public by other authors. In order to simplify the search, the display can be limited to a specific topic/subject in the selection list.

Public collections of others cannot be edited and cannot be deleted. I.e. these icons are not displayed for this purpose.

![](text/en/abcd/example3.png){.center}

## The lobby

After starting the quiz, the lobby view appears. The shown QR code can be used by participants to enter the lobby. 
Alternatively the URL of the lobby or the shown key can be used to join.

![](text/en/abcd/example4.png){.center}


At the beginning of the quiz, the participants are requested to choose a nickname: This has to be at least three characters long. The nickname won't be saved in the database. After participants choose their nicknames, these are shown in the participant list on the right side of the lobby view.

![](text/en/abcd/example5.png){.center}

Participants with inappropiate nicknames can be kicked out of the lobby using the trash bin symbol next their respective nicknames. Now the participants should be seeing a plain page with four buttons (A, B, C, D).

![](text/en/abcd/example6.png){.center}

## Questions and answers

If all participants have arrived in the lobby and chosen a nickname, the host (who is also the moderator of the quiz) can start the quiz using the "start" button: This causes questions and answers to appear one after the other. Participants can only give answers when a question is active.

![](text/en/abcd/example7.png){.center}

## Usability tips

Instead of the blue arrow for scrolling, the space bar can also be used.

After each question, the correct answer is resolved. 

![](text/en/abcd/example8.png){.center}

The current intermediate score is then displayed for the participants.
At the end of the game, an overview of the participants appears, ordered by the number of points they have achieved (one correct answer counts as one point).

![](text/en/abcd/example9.png){.center}

## Hints for other functions

It is possible to change a quiz via the **Edit** function icon.

![](text/en/abcd/example10.png){.center}

**Another function icon is the single test.**

This function allows you to go through the question set as a single person: For those who stayed at home, etc. Then you can see all the questions at a glance and solve them for yourself.

![](text/en/abcd/example11.png){.center}

This link is then passed on for this purpose.

![](text/en/abcd/example12.png){.center}

**Another function icon is Duplicate.**

This function allows you to make a copy of the question collection.

![](text/en/abcd/example13.png){.center}

For this the new name for the collection is then needed.

![](text/en/abcd/example14.png){.center}


**Another function icon is Export.**

This function makes it possible to save the question collection in one's own system.
It is the counterpart to the **Import function**, which allows to activate such a question collection in one's own learning tool instance.

![](text/en/abcd/example15.png){.center}


![](text/en/abcd/example16.png){.center}

The import function then needs the name of the file to be imported.


![](text/de/abcd/example17.png){.center}


**The last function icon is the recycle bin.**

This function allows you to delete the question collection in your own system.

![](text/de/abcd/example18.png){.center}


To avoid erroneous losses, there is a security query.


![](text/en/abcd/example19.png){.center}


