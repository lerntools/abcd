If you do not want the ABCD quiz to be shared via a link, please delete the ticket.

If you want this quiz to only be visible for you, please mark it as **"private"**.

Questions can be moved using the up and down arrows.